package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	List<String> names = new ArrayList<String>(); // Will include names of items.
	List<FridgeItem> storage = new ArrayList<FridgeItem>(); // Will include both name and expiration date.

	public int nItemsInFridge() {	 	
		return names.size();
	}

	public int totalSize() {		
		return 20; // returns the (fixed) max size.	
	}
				
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < 20) {
			names.add(item.getName());
			storage.add(item);
			return true;
		}	
		return false;		
	}
	
	public void takeOut(FridgeItem item) {
		if (nItemsInFridge() == 0) {
			throw new NoSuchElementException();
		} else {
			names.remove(item.getName());
			storage.remove(item);
		}
	}
	
	public void emptyFridge() {
		names.clear();
		storage.clear();
	}

	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
		for (int i = 0; i < nItemsInFridge(); i++) {
			FridgeItem item = storage.get(i);
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		for (FridgeItem expiredItem : expiredFood) {
			names.remove(expiredItem.getName());
			storage.remove(expiredItem);
		}
		return expiredFood;
	}
}